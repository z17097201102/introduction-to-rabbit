package com.itheima;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @program: rabbitmq
 * @description:
 * @author: zhanghz001
 * @create: 2021-01-30 12:06
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rabbitmq-producer.xml")
public class ProducerTtlTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    /**
     * TTL:过期时间
     * 1. 队列统一过期
     * <p>
     * 2. 消息单独过期
     * <p>
     * <p>
     * 如果设置了消息的过期时间，也设置了队列的过期时间，它以时间短的为准。
     * 队列过期后，会将队列所有消息全部移除。
     * 消息过期后，只有消息在队列顶端，才会判断其是否过期(移除掉)
     */
    @Test
    public void testTtl() {
        MessagePostProcessor message = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setExpiration("2000");
                return message;
            }
        };
        rabbitTemplate.convertAndSend("ttl_test_exchange", "ttl.111", "这是一个寂寞的天 ttl");
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                rabbitTemplate.convertAndSend("ttl_test_exchange",
                        "ttl.111", "这是一个寂寞的天 ttl", message);
            } else {
                rabbitTemplate.convertAndSend("ttl_test_exchange",
                        "ttl.111", "这是一个寂寞的天 ttl");
            }
            
        }
    }
    
    /**
     * 发送测试死信消息：
     * 1. 过期时间
     * 2. 长度限制
     * 3. 消息拒收
     */
    @Test
    public void testDeadLetterQueue1() {
        //测试如果超时就会进入到死信交换机和死信队列
        rabbitTemplate.convertAndSend("test_exchange_dlx", "test.dlx.hehe", "这是准备发给死信交换机的数据");
    }
    
    @Test
    public void testDeadLetterQueue2() {
        //在数量超过限制的时候会直接进入到死信队列
        for (int i = 0; i < 20; i++) {
            //死信队列长度为5,长度超过5的直接入死信交换机
            rabbitTemplate.convertAndSend("test_exchange_dlx",
                    "test.dlx.hehe", "这是准备发给死信交换机的数据");
            if (i == 9) {
                MessagePostProcessor message = new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        message.getMessageProperties().setExpiration("2000");
                        return message;
                    }
                };
                rabbitTemplate.convertAndSend("test_exchange_dlx",
                        "test.dlx.hehe", "这是准备发给死信交换机的数据", message);
                
            }
        }
    }
    
    @Test
    public void testDeadLetterQueue3() {   
        //消息拒收
        rabbitTemplate.convertAndSend("test_exchange_dlx", "test.dlx.hehe", 
                "这是准备发给死信交换机的数据" );
    }
    @Test
    public void testDeayQueue() {
        //1.发送订单消息。 将来是在订单系统中，下单成功后，发送消息
        rabbitTemplate.convertAndSend("order_exchange","order.111" ,"订单生产了");
    }
}
