package com.itheima;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @program: rabbitmq
 * @description:
 * @author: zhanghz001
 * @create: 2021-01-27 11:48
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rabbitmq-producer.xml") 
public class ProducerTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Test
    public void testHelloWorld(){
        //默认的交换机不要写
        rabbitTemplate.convertAndSend( "spring_queue","这是helloworld");
    } 
    @Test
    public void testFanout(){
        rabbitTemplate.convertAndSend("spring_fanout_exchange","","fanout pub/sub");
    } 
    @Test
    public void testTopic(){
        rabbitTemplate.convertAndSend("spring_topic_exchange",
                "heima.aaa.bbb","topic 模式  spring_topic_queue_star");
        rabbitTemplate.convertAndSend("spring_topic_exchange",
                "heima.bbb","topic 模式  spring_topic_queue_well spring_topic_queue_star"); 
        rabbitTemplate.convertAndSend("spring_topic_exchange",
                "itcast.111.222","topic 模式  spring_topic_queue_well2");
    }
}
