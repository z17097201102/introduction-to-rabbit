package com.itheima.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: rabbitmq
 * @description:
 * @author: zhanghz001
 * @create: 2021-01-27 16:27
 **/
@Configuration
public class RabbitMQConfig {
    public static final String QUEUE_NAME = "boot_topic_queue";
    public static final String EXCHANGE_NAME = "boot_topic_exchange";
    
    @Bean("exchange")
    public Exchange exchange() {
        return ExchangeBuilder.topicExchange(EXCHANGE_NAME).durable(true).build();
    }
    
    @Bean("queue")
    public Queue queue() {
        return QueueBuilder.durable(QUEUE_NAME).autoDelete().build();
    }
    
    @Bean
    public Binding binding(@Qualifier("queue") Queue queue, @Qualifier("exchange") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("boot.#").noargs();
    }
}
