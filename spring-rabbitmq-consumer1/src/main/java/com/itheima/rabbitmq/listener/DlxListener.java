package com.itheima.rabbitmq.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

/**
 * @program: rabbitmq
 * @description:
 * @author: zhanghz001
 * @create: 2021-01-30 16:21
 **/
public class DlxListener implements ChannelAwareMessageListener {
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            //要返回拒绝,并且把requeue 变成false,表示不重回队列
            System.out.println(new String(message.getBody()));
            channel.basicNack(deliveryTag, true, false);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            
        }
    }
}
