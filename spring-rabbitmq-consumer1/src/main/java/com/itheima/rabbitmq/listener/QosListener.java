package com.itheima.rabbitmq.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

import java.util.concurrent.TimeUnit;

/**
 * @program: rabbitmq
 * @description:
 * @author: zhanghz001
 * @create: 2021-01-30 09:58
 
 * Consumer 限流机制
 *  1. 确保ack机制为手动确认。
 *  2. listener-container配置属性
 *      perfetch = 1,表示消费端每次从mq拉去一条消息来消费，直到手动确认消费完毕后，才会继续拉去下一条消息。
 */
public class QosListener implements ChannelAwareMessageListener {
    @Override
    public void onMessage(Message message, Channel channel) throws Exception { 
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            // TimeUnit.SECONDS.sleep(1);
            System.out.println(new String(message.getBody()));
            // channel.basicAck(deliveryTag, true);
        } catch (Exception e) {
            // channel.basicNack(deliveryTag, true, true);
        } finally {
            
        }
    }
}
