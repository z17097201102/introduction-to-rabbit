package com.itheima.rabbitmq.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

import java.util.concurrent.TimeUnit;

/**
 * @program: rabbitmq
 * @description:
 * 1设置手动签收acknowledge="manual"
 * 
 * @author: zhanghz001
 * @create: 2021-01-28 22:22
 **/

public class AckListener implements ChannelAwareMessageListener {
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            TimeUnit.SECONDS.sleep(1);
            
            System.out.println(new String(message.getBody()));
            // int i=1/0;
            //3. 手动签收
            channel.basicAck(deliveryTag, true);
        } catch (Exception e) {
            
            //4.拒绝签收
            /*
            第三个参数：requeue：重回队列。如果设置为true，
            则消息重新回到queue，broker会重新发送该消息给消费端
             */
            channel.basicNack(deliveryTag, true, true);
        } 
        //channel basicAck
    }
}
